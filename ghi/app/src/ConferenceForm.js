import React, { useEffect, useState } from 'react';

function ConferenceForm () {
  const [locations, setLocations] = useState([]);

  const [name, setName] = useState('');
  const [starts, setStart] = useState('');
  const [ends, setEnd] = useState('');
  const [description, setDescription] = useState('');
  const [max_presentations, setMaxPresentations] = useState('');
  const [max_attendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleStartChange = (event) => {
    const value = event.target.value;
    setStart(value);
  };

  const handleEndChange = (event) => {
    const value = event.target.value;
    setEnd(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  };

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations';

    const response = await fetch(url);
    if (response.ok) {
      const data =await response.json();
      // console.log(data)
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

      const data = {};

      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = description;
      data.max_presentations = max_presentations;
      data.max_attendees = max_attendees;
      data.location = location;


      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');

        };
    }

  return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control" value={starts} />
              <label htmlFor="start">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control" value={ends} />
              <label htmlFor="ends">End Date</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" rows="6" className="form-control" value={description} ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresentationsChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={max_presentations} />
              <label htmlFor="max_presentations">Max number of presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={max_attendees} />
              <label htmlFor="max_attendees">Max number of attendees</label>
            </div>
            <div className="mb-3">
              <select required onChange={handleLocationChange} name="location" id="location" className="form-select">
                <option value="">Location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  );
}

  export default ConferenceForm;
