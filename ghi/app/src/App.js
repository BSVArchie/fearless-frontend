import { BrowserRouter, Routes, Route } from "react-router-dom";

import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import AttendConferenceForm from './AttendConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route index element={<MainPage />}></Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/list" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />}/>
        </Routes>
      </div>
      </BrowserRouter>
  );
}

export default App;



// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           {/* Edit <code>src/App.js</code> and save to reload. */}
//           My code reloads
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
